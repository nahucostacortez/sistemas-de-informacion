package ejemplo;

import java.sql.*;

public class Connect {
	
	private static final String BDPATH = "C:/Users/Samu/Jworkspace/Ejemplo/BD/EjemploBD.accdb";
	
    public String connectionStatus = null;
    private Connection con = null;
    
    public Connect() throws SQLException{
        try 
        {
            con = DriverManager.getConnection("jdbc:ucanaccess://" + BDPATH + ";memory=true");
        }
        catch (SQLException e)
        {
            connectionStatus = String.format("Conexion err�nea: " + e.getMessage());
            // TODO: Clase log.java que guarde a fichero todas las excepciones con su mensaje tracer, la clase, etc...
        }
    }
    
    /**
     * Obtiene una instancia de la conexi�n, est� establecida o no.
     * @return Instancia de la conexi�n.
     */
    public Connection getCon() {
        return con;
    }
}
