package ejemplo;

import java.sql.SQLException;
import java.util.Scanner;

public class Presentacion {

	public static void main(String[] args) throws SQLException {
		
		// TODO: Por cada conexi�n de un cliente creamos un objeto logica de negocio que nos crear� un objeto conexi�n.
		Negocio logica = new Negocio();
		Scanner s = new Scanner(System.in);
		
		// TODO: La clase main seria el formulario (UI)
		System.out.print("Nombre del Socio con ID: ");
		int id = s.nextInt();
		System.out.print("ID: " + id);
		
		// Realizamos la petici�n del nombre a traves de la capa de logica de negocio. 
		String nombre = logica.getName(id);
		System.out.println(": " + nombre);
			
		System.out.println("\n/FIN1/\n");
		
		System.out.println("Reservas: ");
		System.out.println("ID_Reserva\tID_Cancha\tN_Socio\tFecha");
		String [][] reservas = logica.getReservas();
		for (int i=0; i<reservas.length; i++) {
				System.out.printf("%s\t%s\t%s\t%s\n",reservas[i][0],reservas[i][1],reservas[i][2],reservas[i][3]);			
		}
		
		System.out.println("\n/FIN2/\n");
	}
}
