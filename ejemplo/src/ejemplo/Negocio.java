package ejemplo;

import java.sql.*;

public class Negocio { // Actualmente Negocio + Persistencia

	private Connect datos = null;
	private Connection con = null;
	public String runStatus = null;

	public Negocio() throws SQLException {
		datos = new Connect(); // Objeto de la clase conexi�n.
		con = datos.getCon(); // Objeto DAO
		
		/*
 		 * TODO: Objeto DAO no es la mejor soluci�n ya que es preferible hacer una conexi�n por petici�n,
 		 *  por tanto habria que hacer un con.close() al final de cada petici�n (c�digo comentado)
 		 *  la mejor opci�n ser� una capa que realice la conexi�n y la petici�n todo en la misma capa.
 		 *  Despues la capa de Negocio tomar� los resultados de las peticiones y los mostrar� a la capa de
 		 *  presentaci�n como vea oportuno.
 		 */
	}

	/**
	 * Consulta para obtener el nombre de un socio a partir de su identificador de socio.
	 * @param id Identificador del socio, NSocio.
	 * @return El nombre del socio.
	 */
	public String getName(int id) {
		Statement stmt = null;
		ResultSet rs = null;
		String name = null;
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery("SELECT Nombre FROM Socio WHERE NSocio="+id);
			while (rs.next()) {
				name = rs.getString("Nombre");
			}
		}
		catch (SQLException e) {
			runStatus = String.format("Error en consulta: %s", e.getMessage()); 
		}
		finally {
			if (rs != null) try { rs.close(); } catch (SQLException e) { }
			if (stmt != null) try { stmt.close(); } catch (SQLException e) { }
			// if (con != null) try { con.close(); } catch (SQLException e) { }
		}
		return name;
	}
	
	/**
	 * Consulta para obtener todas las filas de la tabla Reservas
	 * @return Una matriz de cadenas, filas para las filas de la tabla y columnas para las columnas de atributos.
	 */
	public String[][] getReservas() {
		Statement stmt = null;
		ResultSet rs = null;
		int idReserva = -1;
		int nSocio = -1;
		int idCancha = -1;
		String fecha = null;
		String[][] reservas = new String[2][]; // Esto seria una estructura, dado que no sabemos el n� de filas.
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery("SELECT * FROM Reservas");
			int i = 0;
			// "rs.next()" avanzar� por las distintas filas de la tabla.
			while (rs.next()) {
				idReserva = rs.getInt("IDReserva");
				nSocio = rs.getInt("NSocio");
				idCancha = rs.getInt("IDCancha");
				fecha = rs.getString("Fecha");
				reservas[i] = new String[] {""+idReserva,""+idCancha,""+nSocio,""+fecha};
				i++;
			}
		}
		catch (SQLException e) {
			runStatus = String.format("Error en consulta: %s", e.getMessage()); 
		}
		finally {
			if (rs != null) try { rs.close(); } catch (SQLException e) { }
			if (stmt != null) try { stmt.close(); } catch (SQLException e) { }
			// if (con != null) try { con.close(); } catch (SQLException e) { }
		}
		return reservas;
	}
}
